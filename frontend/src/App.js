import './App.css'

import PublicMessages from './components/PublicMessages'

export default function App() {
  return <PublicMessages />
}
