import Echo from 'laravel-echo'
import Pusher from 'pusher-js'
import axios from 'axios'

//Set base url cho app
axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;
window.Pusher = Pusher

//Set up broadcast echo
const BroadcastEcho = new Echo({
  broadcaster: 'pusher',
  key: process.env.REACT_APP_ABLY_KEY,
  wsHost: 'realtime-pusher.ably.io',
  wsPort: 443,
  disableStats: true,
  encrypted: true,
});

export { axios, BroadcastEcho }