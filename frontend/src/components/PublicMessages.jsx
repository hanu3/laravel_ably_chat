import { BroadcastEcho, axios } from '../_services/ably'
import React, { useEffect } from "react"

import { useRef } from "react"

const user = prompt('Enter username');

const PublicMessages = () => { 
  
  const ref = useRef(null)

  const [recipientMessages, setRecipientMessages] = React.useState([])

  useEffect(() => { 
    BroadcastEcho.channel('laravel_ably_chat')
      .subscribed(e => console.log('subscribed'))
      .listen('.message.sent', e => { 
        setRecipientMessages(prevState => { 
          return [
            ...prevState,
            {
              user: e.user,
              message: e.message,
              timeSent: e.timeSent
            }
          ]
        })
      })
  }, [])

  const handleMessageSent = async (e) => {
    e.preventDefault()
    if (ref.current.value !== "") { 
      const res = await axios.post('/api/message/sent', {
        user,
        message: ref.current.value,
        timeSent: new Date()
      })
      ref.current.value = "";
    }
  }

  return (
    <>
      {!user && <div>Please enter your username</div>}
      {user &&
        <div className="main">
          <div className="head">
            <div className="title">Public chat app</div>
            <img className="close" src="img/close.svg" />
          </div>
          <div className="chat">
            <div className='chat-messages'>
            {recipientMessages.map(message => (
              <div className={`bubble bubble-${message.user !== user ? 'left': 'right'}`} key={message.message}>
                <div className="hour">{message.user} - {message.timeSent}</div>
                <div className="bubble-content bubble-content-left">
                  <p>{message.message}</p>
                </div>
              </div>
            ))}
            </div>
            <form className="write-section" onSubmit={handleMessageSent}>
              <input
                className="type-text"
                type="text"
                placeholder={`[${user}] Enter your name`}
                ref={ref}
              />
              <button className="button" type="submit">send</button>
            </form>
          </div>
        </div>
      }
    </>
  )
}

export default PublicMessages